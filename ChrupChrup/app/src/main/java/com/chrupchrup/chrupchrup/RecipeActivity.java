package com.chrupchrup.chrupchrup;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chrupchrup.chrupchrup.core.Ingredient;
import com.chrupchrup.chrupchrup.core.Recipe;
import com.chrupchrup.chrupchrup.core.Step;
import com.chrupchrup.chrupchrup.core.Timer;

public class RecipeActivity extends AppCompatActivity {
    private ListView listView;
    private Recipe recipe = null;
    private NotificationManager nNM;
    private final int NOTIFICATION_ID = 0x90679;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_ing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recipe = MainActivity.chosenRecipe;

        nNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nNM.cancel(NOTIFICATION_ID);

        listView = (ListView) findViewById(R.id.listView2);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView t = (TextView) view;
                if ((t.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) == 0) t.setPaintFlags(t.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                else t.setPaintFlags(t.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }
        });

        String[] items = new String[recipe.ingredients.size()];
        for (int i = 0; i < items.length; i++)
            items[i] = recipe.ingredients.get(i).ingredientName + " - " + recipe.ingredients.get(i).value + " " + recipe.ingredients.get(i).unit;

        adapter = new ArrayAdapter<String>(this, R.layout.simplest_list_layout, items);
        listView.setAdapter(adapter);

        Button makeShoppingList = (Button) findViewById(R.id.button);
        makeShoppingList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                PendingIntent contentIntent = PendingIntent.getActivity(RecipeActivity.this, 0, new Intent(RecipeActivity.this, RecipeActivity.class), 0);

                // Add permanent notification
                Notification n  = new Notification.Builder(RecipeActivity.this)
                        .setContentTitle("Shopping list - " + recipe.name)
                        .setContentText("Shopping list")
                        .setOngoing(true)
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .getNotification();
                nNM.notify(NOTIFICATION_ID, n);
            }
        });

        Button next = (Button) findViewById(R.id.button2);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new RecipeView(0);
            }
        });
    }

    private class RecipeView {
        private int currentStepID = 0;
        private Step currentStep;
        private Button previousButton, nextButton, timer, video;
        private TextView description;
        private ImageView image;

        private Timer.TimerCallback timerCallback = new Timer.TimerCallback() {

            @Override
            public void tick(long timeLeft) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        timer.setText(formatTimerString(currentStep.getTime()));
                    }
                });
            }

            @Override
            public void end() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(RecipeActivity.this, "END!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        public RecipeView (int stepNumber) {
            setContentView(R.layout.activity_recipe);

            currentStepID = stepNumber;
            currentStep = recipe.steps.get(currentStepID);

            previousButton = (Button) findViewById(R.id.button4);
            previousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentStepID == 0) setContentView(R.layout.activity_recipe_ing);
                    else new RecipeView(currentStepID - 1);
                }
            });

            nextButton = (Button) findViewById(R.id.button3);
            if (currentStepID == (recipe.steps.size()-1)) nextButton.setVisibility(View.INVISIBLE);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new RecipeView(currentStepID + 1);
                }
            });

            timer = (Button) findViewById(R.id.button5);
            if (!currentStep.isTimes()) timer.setVisibility(View.INVISIBLE);
            else {

                timer.setText(formatTimerString(currentStep.getTime()));
            }

            timer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    currentStep.setTimerCallback(timerCallback);
                    if (!currentStep.isTimerRunning()) currentStep.startTimer();
                }
            });

            video = (Button) findViewById(R.id.button6);
            if (currentStep.getVideoUrl() == "") video.setVisibility(View.INVISIBLE);

            description = (TextView) findViewById(R.id.textView);
            description.setText(currentStep.getDescription());

            image = (ImageView) findViewById(R.id.imageView);
        }

        private String formatTimerString(long time) {

            long seconds = (int) (time / 1000) % 60;
            long minutes = (int) ((time / (1000*60)) % 60);
            long hours   = (int) ((time / (1000*60*60)) % 24);

            String hoursS = Long.toString(hours);
            if (hours < 10) hoursS = "0" + hoursS;

            String minutesS = Long.toString(minutes);
            if (minutes < 10) minutesS = "0" + minutesS;

            String secondsS = Long.toString(seconds);
            if (seconds < 10) secondsS = "0" + secondsS;

            String toReturn = hoursS + ":" + minutesS + ":" + secondsS;
            return toReturn;
        }
    }
}
