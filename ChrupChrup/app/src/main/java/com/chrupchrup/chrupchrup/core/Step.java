package com.chrupchrup.chrupchrup.core;

import java.util.ArrayList;

/**
 * Created by Dawid Worek on 2016-03-18.
 */
public class Step {

    private int no;
    private String des = "";
    private Timer timer;
    private ArrayList<String> photos;
    private String videoUrl;

    public Step(int number, String description, Timer timer, ArrayList<String> photosUrl, String videoUrl) {

        this.no = number;
        this.des = description;
        this.photos = photosUrl;
        this.timer = timer;
        this.videoUrl = videoUrl;
    }

    public String getDescription () {

        return des;
    }

    public void downloadPhotos() {

        for (String photo : photos) {

        }
    }

    public int getNumber() {

        return no;
    }

    public void startTimer() {

        if (timer != null) timer.start();
    }

    public long getTime() {

        if (timer != null) return timer.getTime();
        else return 0L;
    }

    public boolean isTimes () {
        return (timer != null);
    }

    public String getVideoUrl () {
        return videoUrl;
    }

    public void setTimerCallback(Timer.TimerCallback callback) {
        if (timer != null) timer.setCallback(callback);
    }

    public boolean isTimerRunning () {
        return timer.isAlive();
    }

    public void openVideoURL() {

    }
}
