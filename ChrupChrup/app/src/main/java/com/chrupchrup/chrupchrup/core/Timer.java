package com.chrupchrup.chrupchrup.core;


/**
 * Created by Dawid on 2016-03-18.
 */
public class Timer extends Thread {
    private long time;
    private TimerCallback timerCallback = null;

    public Timer (long milliseconds) {

        time = milliseconds;
    }

    public void run () {


        --time;

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (time > 0) if (timerCallback != null) timerCallback.tick(time);
        else if (timerCallback != null) timerCallback.end();
    }

    public long getTime () {
        return time;
    }

    public void setCallback(TimerCallback timerCallback) {
        this.timerCallback = timerCallback;
    }

    public interface TimerCallback {

        void tick(long timeLeft);
        void end();
    }
}
