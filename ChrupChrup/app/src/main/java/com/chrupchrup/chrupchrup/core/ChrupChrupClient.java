package com.chrupchrup.chrupchrup.core;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Dawid Worek on 2016-03-18.
 */
public class ChrupChrupClient {

    private final String URL_ADDRESS = "http://chrupchrupsite.96.lt/";

    public ChrupChrupClient() {

    }

    public ArrayList<Recipe> search(String stringToSearch) throws IOException {

        String value = "";
        String[] strs = stringToSearch.split(" ");
        for (String s : strs) value += s + ";";

        return getRecipes(URL_ADDRESS + "komunikacja.php?page=search&value=" + value);
    }

    public ArrayList<Recipe> getMainPage () throws IOException {
        return getRecipes(URL_ADDRESS + "komunikacja.php?page=main");
    }

    private ArrayList<Recipe> getRecipes(String url) throws IOException {
        ArrayList<Recipe> toReturn = new ArrayList<>();

        URLConnection connection = new URL(url).openConnection();
        InputStream response = connection.getInputStream();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            Log.e("CC", "Error while parsing " + e.getMessage());
            return null;
        }
        try {
            doc = builder.parse(response);
        } catch (SAXException e) {
            Log.e("CC", "Error while reading " + e.getMessage());
            return null;
        }

        Element root = doc.getDocumentElement();
        NodeList nList = root.getElementsByTagName("przepis");
        for (int i = 0; i < nList.getLength(); i++) {
            ArrayList<Ingredient> ingredients = new ArrayList<>();
            ArrayList<Step> steps = new ArrayList<>();

            Node nNode = nList.item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                // Dane o przepisie
                String title = eElement.getAttribute("tytul");
                String author = eElement.getAttribute("autor");
                float stars = Float.parseFloat(eElement.getAttribute("ocena"));
                int id = Integer.parseInt(eElement.getAttribute("id"));

                // Skladniki
                NodeList l = eElement.getElementsByTagName("skladnik");
                for (int j = 0; j < l.getLength(); j++) {
                    Element e = (Element) l.item(j);

                    ingredients.add(new Ingredient(e.getAttribute("nazwa"), Float.parseFloat(e.getAttribute("ile")),  e.getAttribute("jednostka")) );
                }

                // Kroki
                l = eElement.getElementsByTagName("step");
                for (int j = 0; j < l.getLength(); j++) {
                    Element e = (Element) l.item(j);

                    int number = Integer.parseInt(e.getAttribute("numer"));
                    Timer timer = null;
                    ArrayList<String> photos = new ArrayList<>();
                    String videoUrl = "";

                    String des = e.getElementsByTagName("opis").item(0).getFirstChild().getNodeValue();
                    NodeList list = e.getElementsByTagName("video");
                    if (list.getLength() > 0)
                        videoUrl = list.item(0).getFirstChild().getNodeValue();
                    list = e.getElementsByTagName("timer");
                    if (list.getLength() > 0)
                        try {
                            timer = new Timer(Long.parseLong(list.item(0).getFirstChild().getNodeValue()) * 1000);
                        } catch (java.lang.NumberFormatException ex) {
                            timer = null;
                        }

                    list = e.getElementsByTagName("zdjecie");
                    for (int k = 0; k < list.getLength(); k++) {
                        Element z = (Element) list.item(k);

                        photos.add(z.getFirstChild().getNodeValue());
                    }

                    steps.add(new Step(number, des, timer, photos, videoUrl));
                }
                toReturn.add( new Recipe(id, stars, title, ingredients, author, steps) );
            }
        }

        return toReturn;
    }
}
