package com.chrupchrup.chrupchrup.core;

/**
 * Created by Dawid on 2016-03-18.
 */
public class Ingredient {

    public String ingredientName;
    public float value;
    public String unit;

    public Ingredient(String name, float value, String unit) {

        this.ingredientName = name;
        this.unit = unit;
        this.value = value;
    }
}
