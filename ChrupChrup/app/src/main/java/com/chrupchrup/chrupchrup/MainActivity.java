package com.chrupchrup.chrupchrup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chrupchrup.chrupchrup.core.ChrupChrupClient;
import com.chrupchrup.chrupchrup.core.Recipe;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ChrupChrupClient chrupChrupClient = new ChrupChrupClient();
    private ProgressDialog progress;
    private ListView listView;
    private RecipesArrayAdapter arrayAdapter = null;

    public static Recipe chosenRecipe = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                chosenRecipe = arrayAdapter.getItem(position);

                Intent intent = new Intent(MainActivity.this, RecipeActivity.class);
                startActivity(intent);
            }
        });



        ContentDownloader cd = new ContentDownloader();
        cd.execute("", "", "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Searching");

                // Set up the input
                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String textToSearch = input.getText().toString();

                        ContentDownloader cd = new ContentDownloader();
                        cd.execute(textToSearch, textToSearch, textToSearch);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private class ContentDownloader extends AsyncTask<String, String, String> {
        private ArrayList<Recipe> returnedList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress = ProgressDialog.show(MainActivity.this, getString(R.string.loading), getString(R.string.wait));
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected String doInBackground(String... urls) {

            if (urls[0] == "") {

                try {
                    returnedList = chrupChrupClient.getMainPage();
                } catch (IOException e) {
                    returnedList = null;
                    Log.e("CC", "Error while downloading");
                }
            } else {

                try {
                    returnedList = chrupChrupClient.search(urls[0]);
                } catch (IOException e) {
                    returnedList = null;
                    Log.e("CC", "Error while downloading");
                }
            }

            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String unused) {
            progress.dismiss();

            if (returnedList != null) {

                arrayAdapter = new RecipesArrayAdapter(MainActivity.this, returnedList);
                listView.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();
                listView.invalidateViews();

                Log.i("CC", "Returned list: " + String.valueOf(returnedList.size()));
            } else {
                Log.i("CC", "No returned list");
            }
        }

    }

    private class RecipesArrayAdapter extends ArrayAdapter<Recipe> {

        private final Context context;
        private final ArrayList<Recipe> values;

        public RecipesArrayAdapter(Context context, ArrayList<Recipe> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.list_element, parent, false);
            TextView title = (TextView) rowView.findViewById(R.id.firstLine);
            TextView stars = (TextView) rowView.findViewById(R.id.secondLine);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

            Recipe r = values.get(position);
            title.setText(r.name + " - " + r.author);
            stars.setText("Rating: " + Float.toString(r.stars));

            return rowView;
        }
    }
}
