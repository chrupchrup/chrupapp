package com.chrupchrup.chrupchrup.core;

import java.util.ArrayList;

/**
 * Created by Dawid on 2016-03-18.
 */
public class Recipe {
    public int id;
    public float stars;
    public String name;
    public ArrayList<Ingredient> ingredients;
    public String author;
    public ArrayList<Step> steps;

    public Recipe (int id,
                   float stars,
                   String title,
                   ArrayList<Ingredient> ingredients,
                   String author,
                   ArrayList<Step> steps) {

        this.id = id;
        this.stars = stars;
        this.name = title;
        this.ingredients = ingredients;
        this.author = author;
        this.steps = steps;
    }


}
